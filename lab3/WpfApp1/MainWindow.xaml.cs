﻿
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net;
using System.IO;
using System.ComponentModel;
using System.Data.Entity;

namespace WpfApp1
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
     
        BackgroundWorker worker = new BackgroundWorker();
        PersonDbContext _db = new PersonDbContext();
        
        async Task<int> AccessTheWebAsync(int number)
        {
            using (HttpClient client = new HttpClient())
            {
                Task<string> getStringTask = client.GetStringAsync("https://pl.wikipedia.org/wiki/Wrocław");

                if (number < 0)

                    throw new ArgumentOutOfRangeException("number", number, "The number must be greater or qeual zero");
                int result = 0;
                while (result < number)
                {
                    result++;
                    await Task.Delay(100);
                }
             
                string urlContents = await getStringTask;

                return urlContents.Length;
            }
        }
        protected void UpdateProgressBlock(string text, TextBlock textBlock)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    textBlock.Text = text;
                });
            }
            catch { }
        }
        class WaitingAnimation
        {
            private int maxNumberOfDots;
            private int currentDots;
            private MainWindow sender;


            public WaitingAnimation(int maxNumberOfDots, MainWindow sender)
            {
                this.maxNumberOfDots = maxNumberOfDots;
                this.sender = sender;
                currentDots = 0;
            }
            public void CheckStatus(Object stateInfo)
            {
                sender.UpdateProgressBlock(
                    "Oczekiwanie na pobranie zawartości strony." +
                    new Func<string>(() => {
                        StringBuilder strBuilder =new StringBuilder(string.Empty);
                for (int i = 0; i < currentDots; i++)
                    strBuilder.Append(".");
                return strBuilder.ToString();
            })(), sender.progressTextBlock
                );

                if(currentDots == maxNumberOfDots)
                currentDots=0;
                else
                currentDots++;
            }
           
    }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        ObservableCollection<Person> people = new ObservableCollection<Person>
        {
           new Person { Name="Zagrzeb", Age=12,Humidity=12 },
           new Person {Name="Paris", Age=20,Humidity=20}
        };


        public ObservableCollection<Person> Items
        {
            get => people;
        }
     

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            
        }

    
        private void AddNewPersonButton_Click(object sender, RoutedEventArgs e)
        {
            Progres_text_block1.Text = "";
            Progres_text_block2.Text = "";
            try
            {
                people.Add(new Person { Age = int.Parse(Temperature_text_block2.Text), Name = City_text_block2.Text, Humidity = int.Parse(Humidity_text_block2.Text), Id = 1 });
                _db.people.Add(new Person()
                {
                    Age = int.Parse(Temperature_text_block2.Text),
                    Name = City_text_block2.Text,
                    Humidity = int.Parse(Humidity_text_block2.Text),
                    Id = 1
                });
            }
            catch (System.FormatException)
            {
                MessageBox.Show("Wpisz prawidłowe dane");
            }
        }

       
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            List<string> cities = new List<string> {
                "London", "Warsaw", "Paris", "London", "Warsaw" };
            for (int i = 1; i <= cities.Count; i++)
            {
                string city = cities[i - 1];

                if (worker.CancellationPending == true)
                {
                    worker.ReportProgress(0, "Cancelled");
                    e.Cancel = true;
                    return;
                }
                else
                {
                    worker.ReportProgress(
                        (int)Math.Round((float)i * 100.0 / (float)cities.Count),
                        "Loading " + city + "...");
                    string responseXML = WeatherConnection.LoadDataAsync(city).Result;
                    WeatherDataEntry result;

                    using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseXML)))
                    {
                        result = ParseWeather_xmlReader.Parse(stream);
                    }
                    Thread.Sleep(2000);
                }
            }
            worker.ReportProgress(100, "Done");
        }

        private void FinalNumberTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            XML_progress_bar.Value = e.ProgressPercentage;
            XML_text_block.Text = e.UserState as string;
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private async void XML_button_Click(object sender, RoutedEventArgs e)
        {
            Progres_text_block1.Text = "";
            Progres_text_block2.Text = "";
            string Chosen_city = City_text_block.Text.ToString();

            string responseXML = await WeatherConnection.LoadDataAsync(Chosen_city); 
            WeatherDataEntry result;

            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(responseXML)))
            {
                result = ParseWeather_xmlReader.Parse(stream);
                Items.Add(new Person()
                {
                    Name = result.City,
                    Age = (int)Math.Round(result.Temperature),
                    Humidity = (int)Math.Round(result.Humidity),
                });
                _db.people.Add(new Person()
                {
                    Name = result.City,
                    Age = (int)Math.Round(result.Temperature),
                    Humidity = (int)Math.Round(result.Humidity),
                    Id = 1
                });
            }

            if (worker.IsBusy != true)
                worker.RunWorkerAsync();
        }
        private void XML_progress_bar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {

            if (worker.WorkerSupportsCancellation == true)
            {
                XML_text_block.Text = "Cancelling...";
                worker.CancelAsync();
            }
        }

        private void City_text_block_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Uploaddata_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string num = ID_text.Text;
                var dRpw = _db.people.Where(w => w.Name == num).FirstOrDefault();
                people.Add(new Person { Name = dRpw.Name, Age = dRpw.Age, Humidity = dRpw.Humidity, Id = 1 });
                Progres_text_block2.Text = "Uploaded from db";
            }
            catch (System.FormatException)
            {
                MessageBox.Show("Nie ma w bazie danych");
            }
        }

        private void Updatedata_Click(object sender, RoutedEventArgs e)
        {
               
            _db.SaveChanges();
            Progres_text_block1.Text = "Data base has been updated";
            
        }

        private void ID_text_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}



