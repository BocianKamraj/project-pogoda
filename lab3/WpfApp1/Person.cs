﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Data.Entity;
using System;

namespace WpfApp1
{
    public class Person
    {
        public int Id {get;set;}
        public string Name { get; set; }
        public int Age { get; set; }
        public int Humidity{ get; set; }
    }
    public class PersonDbContext : DbContext
    {
        public DbSet<Person> people { get; set; }


    }
}
