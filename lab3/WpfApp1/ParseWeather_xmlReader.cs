﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WpfApp1
{
  public  class ParseWeather_xmlReader
    {
        public static WeatherDataEntry Parse(System.IO.Stream stream)
        {
            XmlTextReader reader = new XmlTextReader(stream);
            WeatherDataEntry result = new WeatherDataEntry()
            {
                City = string.Empty,
                Temperature = float.NaN,
                Humidity = float.NaN
            };
            while (reader.Read())
            {
                Console.WriteLine(reader.Name);
                Console.WriteLine((reader.GetAttribute("value")));
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "city":
                                result.City = reader.GetAttribute("name");
                                break;
                            case "temperature":
                                result.Temperature =
                                    float.Parse(
                                        reader.GetAttribute("value"),
                                        System.Globalization.CultureInfo.InvariantCulture)-273;
                                break;
                            case "humidity":
                                result.Humidity =
                                    float.Parse(
                                        reader.GetAttribute("value"),
                                        System.Globalization.CultureInfo.InvariantCulture);
                                break;
                        }
                        break;
                }
            }
            return result;
        }
    }
}
                        
                
            
        
    

